class AppOfTheYear {
  AppOfTheYear({
    required this.name,
    required this.sector,
    required this.developer,
    required this.year,
  });
  final String name;
  final String sector;
  final String developer;
  final int year;

// Print description of the classes and built in function to print the app names in
// all caps
  void printDescription() {
    print(
        """${name.toUpperCase()} app. Developed by $developer which is in the $sector 
    sector/category and won the MTN business App of the Year Awards in $year.
    """);
  }
}

// Defining the objects ib the class of the App of the Year Awards for each year.
void main() {
  final app1 = AppOfTheYear(
      name: "FNB banking", developer: "FNB", sector: "Banking", year: 2012);
  final app2 = AppOfTheYear(
      name: "SnapScan",
      developer: "SnapScan ",
      sector: "Payment Getway",
      year: 2013);
  final app3 = AppOfTheYear(
      name: "LIVE Inspect",
      developer: "LIVE Inspect",
      sector: "Solutions",
      year: 2014);
  final app4 = AppOfTheYear(
      name: "WumDrop", developer: "WumDrop", sector: "Logistics", year: 2015);
  final app5 = AppOfTheYear(
      name: "Domestly",
      developer: "Domestly",
      sector: "Cleaning Services",
      year: 2016);
  final app6 = AppOfTheYear(
      name: "Shyft",
      developer: "Standard bank",
      sector: "Digital Wallet",
      year: 2017);
  final app7 = AppOfTheYear(
      name: "Khula ecosystem",
      developer: "Khula ecosystem",
      sector: "Supply Chain",
      year: 2018);
  final app8 = AppOfTheYear(
      name: "Naked Insurance",
      developer: "Naked Insurance",
      sector: "Insuretec",
      year: 2019);
  final app9 = AppOfTheYear(
      name: "EasyEquities",
      developer: "First World Trader (Pty) Ltd",
      sector: "Share Trading",
      year: 2020);
  final app10 = AppOfTheYear(
      name: "Ambani Africa",
      developer: "Ambani Africa",
      sector: "Tech",
      year: 2021);

  app1.printDescription();
  app2.printDescription();
  app3.printDescription();
  app4.printDescription();
  app5.printDescription();
  app6.printDescription();
  app7.printDescription();
  app8.printDescription();
  app9.printDescription();
  app10.printDescription();
}
