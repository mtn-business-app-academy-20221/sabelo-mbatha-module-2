import "dart:io";

Map<String, dynamic> people = {};

void main() {
  while (true) {
    stdout.write("What is your name? ");
    final input1 = stdin.readLineSync();
    // Checking if the person was added before now.
    if (people.containsValue("$input1")) {
      print("The person is already on saved!");
      break;
    } else {
      // Adding the other requested information.
      var person1 = {"name": "$input1"};
      stdout.write("What is your favorite app? ");
      final input2 = stdin.readLineSync();
      person1["favorite app"] = "$input2";
      stdout.write("What is your City? ");
      final input3 = stdin.readLineSync();
      person1["City"] = "$input3";
      people = {...people, ...person1};
      print(people);
    }
  }
}
