import 'dart:collection';

void main() {
  var mtnApps = <dynamic, dynamic>{
    2012: "FNB banking app",
    2013: "SnapScan ",
    2014: "LIVE Inspect",
    2015: "WumDrop ",
    2016: "Domestly ",
    2017: "Shyft",
    2018: "Khula ecosystem",
    2019: "Khula ecosystem",
    2020: "EasyEquities ",
    2021: "Ambani Africa"
  };

  // Converting Map to List then sorting with the years.
  var mtnAppsList = mtnApps.keys.toList(growable: false)
    ..sort((k1, k2) => mtnApps[k1].compareTo(mtnApps[k2]));
  LinkedHashMap sortedMap = new LinkedHashMap.fromIterable(mtnAppsList,
      key: (k) => k, value: (k) => mtnApps[k]);
  print("""Apps by name with the years of production: 
  $sortedMap
  """);

  // Converting Map to List then sorting withouth the years.
  var okay = [
    mtnApps[2012],
    mtnApps[2013],
    mtnApps[2014],
    mtnApps[2015],
    mtnApps[2016],
    mtnApps[2017],
    mtnApps[2018],
    mtnApps[2019],
    mtnApps[2020],
    mtnApps[2021]
  ];
  okay.sort();
  print("""Apps by name without years: 
$okay
""");

  // Getting the winning app of 2017 and the winning app of 2018
  var winningSeventeen = mtnApps[2017];
  var winningEighteen = mtnApps[2018];
  print("""
The winning app of 2017 is the $winningSeventeen app.
The winning app of 2018 is the $winningEighteen app.
""");

  // Printing the number of apps on the MTN App of the year.
  print("The total number of apps is ${mtnAppsList.length}");
}
